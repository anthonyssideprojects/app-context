# CONTAINER, DEPENDENCY, AND IOC
*  ~~What is dependency injection and what are the advantages?~~
  * System which decouples the client from its dependencies
  * The responsibility for constructing and config the deps is passed over to a dep inj container - like spring.
  * Results in looser coupling
  * Clients don't have to know the details of how to construct the objects they rely on.

*  ~~What is an interface and what are the advantages of making use of them in Java?~~
  * Specification for a class
  * Can code to an interface in multiple classes which can then be used interchangeably in code
  * In dep inj case a client can be coded to accept an fully initialized instance of a dependency interface (or rather an instance of the class which implements this interface)


*  ~~What is meant by “application-context” and how do you create one?~~
  * [ApplicationContext](https://spring.io/understanding/application-context) is a central interface in a spring application.  Usually don't create one directly.
  * Implementing classes provide methods for looking up beans,  generic resource loaders (props files etc.), ability to publish events to registered listeners.
  * Can implement ApplicationContextAware or do this:
  ~~~
  ApplicationContext context =
		          new AnnotationConfigApplicationContext(MyApplication.class);
  ~~~
  * Api docs for [ApplicationContext](http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/context/ApplicationContext.html)


*  ~~What is the concept of a “container” and what is its lifecycle?~~
	   Container is essentially the ApplicationContext. Creates beans by reading the config and manages their lifecycle.


*  Dependency injection using Java configuration

*  Dependency injection in XML, using constructor or setter injection

*  ~~Dependency injection using annotations (@Component, @Autowired)~~

*  Component scanning, Stereotypes and Meta-Annotations

*  ~~Scopes for Spring beans. What is the default?~~
  * singleton(default) – Return a single bean instance per Spring IoC container
	* prototype – Return a new bean instance each time when requested
	* request – Return a single bean instance per HTTP request.
	* session – Return a single bean instance per HTTP session.
	* globalSession – Return a single bean instance per global HTTP session.



*  ~~What is an initialization method and how is it declared in a Spring bean?~~
  * Called after the constructor and any setters. Annotate with @PostConstruct

*  ~~What is a destroy method, how is it declared and when is it called?~~
  * Called when the container is exiting. Annotate with @PreDestroy.

*  What is a BeanFactoryPostProcessor and what is it used for?

*  What is a BeanPostProcessor and how is the difference to a BeanFactoryPostProcessor? What do they do? When are they called?

*  Are beans lazily or eagerly instantiated by default? How do you alter this behavior?
  * lazily

*  ~~What does component-scanning do?~~
  * Finds beans for the container to manage. Note Spring boot default annotation includes this component scan for the current directory.


*  What is the behavior of the annotation @Autowired with regards to field injection, constructor injection and method injection?

*  How does the @Qualifier annotation complement the use of @Autowired?
  * If you have more than one bean of a type Qualifier can be used to disambiguate.


*  What is a proxy object and what are the two different types of proxies Spring can create?

*  What is the power of a proxy object and where are the disadvantages?

*  What are the limitations of these proxies (per type)?

*  How do you inject scalar/literal values into Spring beans?

*  How are you going to create a new instance of an ApplicationContext?

*  What is a prefix?

*  What is the lifecycle on an ApplicationContext?

*  ~~What does the @Bean annotation do?~~
  Designates a pojo as a bean to be managed by the ApplicationContext.

*  How are you going to create an ApplicationContext in an integration test or a JUnit test?

*  What do you have to do, if you would like to inject something into a private field?
  Nothing special. Injection is done by proxy generated at run time.

*  What are the advantages of JavaConfig? What are the limitations?

*  ~~What is the default bean id if you only use "@Bean"?~~
  * The method name

*  Can you use @Bean together with @Profile?

*  What is Spring Expression Language (SpEL for short)?

*  What is the environment abstraction in Spring?

*  What can you reference using SpEL?

*  How do you configure a profile. What are possible use cases where they might be useful?

*  ~~How many profiles can you have?~~
  * no limit

*  How do you enable JSR-250 annotations like @PostConstruct?

*  Why are you not allowed to annotate a final class with @Configuration

*  Why must you have a default constructor in your @Configuration annotated class?

*  Why are you not allowed to annotate final methods with @Bean?

*  What is the preferred way to close an application context?

*  How can you create a shared application context in a JUnit test?

*  What does a static @Bean method do?

*  What is a ProperyPlaceholderConfigurer used for?

*  ~~What is @Value used for?~~
  Annotation to provide a default value to a method/value/constructor

*  What is the difference between $ and # in @Value expressions?
  * $ is to select last instance of a match (^ is first)
  * ???

# ~~ASPECT ORIENTED PROGRAMMING~~
*  ~~What is the concept of AOP? Which problem does it solve?~~
  * Add cross cutting concerns to software without polluting original code. Advice is captured separately.

*  ~~What is a pointcut, a join point, an advice, an aspect, weaving?~~
  * Pointcut is the defn of what method the advice is mapped to.
  * A join point is the point in the code that matches the point cut
  * Advice is the code that will be executed when the point cut matches a join point
  * An aspect is part of the code that is not concerned with main business logic and can be kept separate.
  * Weaving is the process of adding advice to the required methods. Performed by spring which creates proxies for appropriately annotated classes. The proxy then calls out to the advice at run time.

*  ~~How does Spring solve (implement) a cross cutting concern?~~
  * As an aspect. Create point cut to target desired join point.

*  ~~Which are the limitations of the two proxy-types?~~
  * CGLIB and JDK Proxy
    jdk proxy is preferred. Will be used whenever your bean implements an interface. If not CGLIB is used.
  * CGLIB won't work with final methods (it is the type of proxy that does not use interfaces - that is why this is relevant).



*  ~~How many advice types does Spring support[5]. What are they used for?~~
  * Before - before a method is called
  * AfterReturning - after a method is called (can access return value). Not if an exception was thrown.

  * After - regardless of whether or not an exception was thrown.

  * AfterThrowing - after a method throws an exception
  * Around - access to both arguments pre call and return val post

*  ~~What do you have to do to enable the detection of the @Aspect annotation?~~
  * Put @EnableAspectJAutoProxy on a configuration class

*  ~~Name three typical [cross cutting concerns](https://en.wikipedia.org/wiki/Cross-cutting_concern).~~
  * Transactions
  * Logging
  * Caching


*  ~~What two problems arise if you don't solve a cross cutting concern via AOP?~~
  * Tangling and
  * Scattering

*  ~~What does @EnableAspectJAutoProxy do?~~
  * Switches on Aspects for Spring

*  ~~What is a named pointcut?~~
  * You can give a name to a point cut (the method that is annotated will not be called - just leave it empty) and then refer to the method name in another Advice to reuse. Lends itself well to logical operations like &&.

*  ~~How do you externalize pointcuts? What is the advantage of doing this?~~
  * Put your named pointcuts in a separate class.
  * Keeps complex pointcut expressions together and makes them reusable.
  * Lends itself well to logical expressions in pointcuts.


*  ~~What is the JoinPoint argument used for?~~
  * Gives access to arguments and methodName

*  ~~What is a ProceedingJoinPoint?~~
  * JoinPoint that allows proceed() to be called. This call allows the method to complete execution.

* ~~What are the five advice types called?~~
  * Before, After, AfterReturning, AfterThrowing, Around

*  ~~Which advice do you have to use if you would like to try and catch exceptions?~~
  * @AfterThrowing


# ~~JDBC, TRANSACTIONS, AND ORM~~
*  ~~What is the difference between checked and unchecked exceptions?~~
  * Checked must be declared or caught. If declared this must be carried up the hierarchy until they are eventually caught.
  * Unchecked exceptions can be caught, but don't have to be (they will propagate if they are not) and can be declared, but don't have to be.

* ~~Why do we (in Spring) prefer unchecked exceptions?~~
 * No need to obey the catch or declare rule, so client does not have to know about how the server implements its functionality. (The throws clause is this information).

*  ~~What is the data access exception hierarchy?~~
  * Normal data access exceptions are things like sql exceptions. These tell the client how the data is being stored (jdbc in this case). Spring DAEs are translations of many of the common data exceptions in order that they be opaque to the client. The original exception is wrapped and so not lost.



*  ~~How do you configure a DataSource in Spring? Which bean is very useful for development/test databases?~~
  * Declare a datasource bean (xml) with url, username and password.
  * EmbeddedDatabaseBuilder is for test databases.


*  ~~What is the Template design pattern and what is the JDBC template?~~
  * Template design patterns implements the skeleton of a process. The individual parts can be swapped out to achieve the desired functionality.
  The JdbcTemplate Allows sql to be executed and results returned without the overhead of creating and managing db connections.


*  ~~What is a callback? What are the three JdbcTemplate callback interfaces described in the notes? What are they used for? (You would not have to remember the interface names in the exam, but you should know what they do if you see them in a code sample).~~
  * A callback is a method which is passed to another method for execution at some future point.
  * for example the rowcallback in the JdbcTemplate allows rows to be processed individually.
  * RowMapper.mapRow() -> Get a single row
  * RowCallbackHandler.processRow() -> Process all rows
  * ResultSetExtractor.extractData() -> Process the entire ResultSet (for example to look at the metadata). You are responsible for iteration.

*  ~~Can you execute a plain SQL statement with the JDBC template?~~
  * Yes

*  Does the JDBC template acquire (and release) a connection for every method called or once per template?



*  Is the JDBC template able to participate in an existing transaction?


*  ~~What is a transaction? What is the difference between a local and a global transaction?~~
  * A transaction is a unit of work. Either all of it is committed, or all rolled back.
  * Local is for a specific resource. eg. db. Global is across multiple resources. Controlled by the app server. Normally jee, but can be spring jta here.


*  ~~Is a transaction a cross cutting concern? How is it implemented in Spring?~~
  * Yes, it is cross cutting. Implemented using AOP.

*  ~~How are you going to set up a transaction in Spring?~~
  * Deploy a PlatformTransactionManager which suits the use case. For example DatasourceTransactionManager.
  * Add the EnableTransactionManagement annotation.
  * Then mark the methods (or class/interface) with @Transactional.

*  ~~What does @Transactional do? What is the PlatformTransactionManager?~~
  * @Transactional marks a method as transactional, or all the methods in a class as transactional.
  * PlatformTransactionManager is the class which implements specific transaction behavior for a specific resource type. Example JMS.

*  What is the TransactionTemplate? Why would you use it?

*  ~~What is a transaction isolation level? How many do we have and how are they ordered?~~
  * Describes how separate transactions interact.
  * READ_UNCOMMITTED - worst level, might be used in high volume environment where data is constantly changing (they why use transactions!). Allows dirty reads.
  * READ_COMMITTED - When data from one transaction is committed it is immediately available to any other transactions. Prevents dirty reads.
  * REPEATABLE_READ - Once a transaction reads a value that value will not be changed by another transaction until the first transacation commits or rolls back. High integrity, but may require locking. Impacts performance.
  * SERAIALIZABLE - Prevents phantom reads.


*  ~~How does the JdbcTemplate support generic queries? How does it return objects and lists/maps of objects?~~
  * By using the RowMapper callback.

*  ~~What does transaction propagation mean?~~
  * If a transactional method calls out to another method which is also transactional propagation describes what happens.

*  ~~What happens if one @Transactional annotated method is calling another @Transactional annotated method on the same object instance?~~
  * Depends on the value of the propagation variable on the second transaction. REQUIRES_NEW creates a new transaction. Otherwise the existing transaction is joined.

*  ~~Where can the @Transactional annotation be used? What is a typical usage if you put it at class level?~~
  * Class or method.
  * At class level is sets the defaults configs - example timeout - at method level these are overridden as appropriate.

*  ~~What does declarative transaction management mean?~~
  * Transactionality of a method is declared in an annotation rather than requiring custom transaction code.

*  ~~What is the default rollback policy? How can you override it?~~
  * Rollback on uncaught runtime exception.
  * use the rollbackFor and noRollback for annotation parameters on @Transactional.

*  ~~What is the default rollback policy in a JUnit test, when you use the
SpringJUnit4ClassRunner and annotate your @Test annotated method with
@Transactional?~~
  * Rollback after the method completes. No need to clean up the database.
  * So @Test and @Transactional on a method do this.
  * Adding @Commit to a test method means that normal commit on completion rules apply.


*  ~~Why is the term "unit of work" so important and why does JDBC AutoCommit
violate this pattern?~~
  * Unit of work is a pattern which remembers what has been done (actions carried out for example). Can be used to know what to roll back and in what order. also used for commit. JDBC AutoCommit commits after every statement is sent to the database. This removes the avility to roll back and the Unit of Work is effectively the current statement only.

*  ~~What does JPA mean - what is ORM? What is the idea behind an ORM?~~
  * Java Persistence Api. Object Relational Mapping. Objects need to be stored in a relational db (assuming you are using one). A mapping needs to be established between the fields and the db columns. This is done under the JPA standard by convention and annotation.

*  ~~What is a PersistenceContext and what is an EntityManager. What is the
relationship between both?~~
  * A PersistenceContext is a group of entities whose persistence is managed together. An EntityManager is a class used to manage the entities with delete, save etc.

*  ~~Why do you need the @Entity annotation. Where can it be placed?~~
  * It tags a class as an entity for persistence with jpa. Placed on class.

*  ~~What do you need to do in Spring if you would like to work with JPA?~~
  * Add the dependency to your pom and update.

*  Are you able to participate in a given transaction in Spring while working with JPA?
  * Yes

*  ~~What is the PlatformTransactionManager?~~
  * It is an interface the implementation of which controls the transactions for an application. There are various types - datasource, jta etc.

*  ~~What does @PersistenceContext do?~~
  * Injects a persistence context into the class.

*  What are disadvantages of ORM? What are the benefits?

*  ~~What is an "instant repository"? (hint: recall Spring Data)~~
  * In mem test db created and popuated for a test or set of tests.

*  ~~How do you define an “instant” repository?~~
  * EmbeddedDatabaseBuilder is the easiest way. Also xml for jdbc:embedded-database.

*  ~~What is @Query used for?~~
  * Allows running a custom query for a finder method. It is a named query.

# SPRING MVC AND THE WEB LAYER
*  ~~MVC is an abbreviation for a design pattern. What does it stand for and what is the idea behind it?~~
  * Model View controller
  * Separate the data from its presentation and the routing of commands.


*  ~~Do you need spring-mvc.jar in your classpath or is it part of spring-core?~~
  * Mostly part of core, but RequestMappings are web and webmvc only. So doable, but unusual.

*  ~~What is the DispatcherServlet and what is it used for?~~
  Servlet which handles all incoming requests and sends them to the appropriate controller.

*  Is the DispatcherServlet instantiated via an application context?

*  What is the root application context? How is it loaded?

*  What is the @Controller annotation used for? How can you create a controller without an annotation?
  * @Controller designates a class as a controller.

*  What is the ContextLoaderListener and what does it do?

*  ~~What are you going to do in the web.xml. Where do you place it?~~
  * Configure servlets, filters etc.
  * in the Web-inf directory.

*  ~~How is an incoming request mapped to a controller and mapped to a method?~~
  * Methods have a RequestMapping annotation which gives the path that corresponds to this method. The RequestMapping can also specify the http method and other criteria.

*  ~~What is the @RequestParam used for?~~
  * Access the parameters on the request. For example stuff?id=3. @RequestParam("id") Long localId) .;

*  ~~What are the differences between @RequestParam and @PathVariable?~~
  PathVariable is used for uritemplate variables. For example: /items/{id}. The PathVarible (id) can be assigned to a variable using (@PathVarible("id") Long localId){}.

* ~~What are some of the valid return types of a controller method?
  String, void, Object~~

*  ~~What is a View and what's the idea behind supporting different types of View?~~
    View is a presentation object.
    Different views can be used for different presentation requirements. For example a jsp for normal desktop web usage.

*  How is the right View chosen when it comes to the rendering phase?

*  ~~What is the Model?~~
  * Map of data passed from controller to view.

*  ~~Why do you have access to the model in your View? Where does it come from?~~
  * View takes data from the model to populate its template.
  Controller creates views

*  What is the purpose of the session scope?

*  What is the default scope in the web context?

*  Why are controllers testable artifacts?

*  ~~What does the InternalResourceViewResolver do?~~
  * Resolves the names of views based on what is returned from the controller. So if jsp views have been configured then the returned string has .jsp appended to find the view.


# ~~SECURITY~~
*  ~~What is the delegating filter proxy?~~
  * Delegating filter proxy is the component which intercepts all requests to a secured resource in spring.

*  ~~What is the security filter chain?~~
  * Set of filters that a web request passes through in order to get to a secured resource (or unsecured if spring sec is configured)

*  ~~In the notes several predefined filters were shown. Do you recall what they did and what order they occurred in?~~

  SecurityContextPersistenceFilter
    Check if the user is logged in and store their login details in the session.
  LogoutFilter
    Log the user out
  UsernamePasswordAuthenticationFilter
    Check if they have supplied a username and password. If not redirect them to login resource.
  ExceptionTranslationFilter
    Catches exceptions from layer below.
  FilterSecuritiyIntercepter
    Make the decision to grant access to the underlying resource based on a lookup of the attributes. If no throw exception up to be dealt with by ExceptionTranslationFilter.


*  ~~Are you able to add and/or replace individual filters?~~
  * Yes - replace by extending the filter to replace
  * Adding before or after users the http.addFilterBefore or http.addFilterAfter methods when configuring.

*  ~~Is it enough to hide sections of my output (e.g. JSP-Page)?~~
  * No

*  ~~Why do you need the intercept-url?~~
  * To secure a specific url.

*  ~~Why do you need method security? What type of object is typically secured at the method level (think of its purpose not its Java type).~~
  * To prevent requests that bypass your security urls. For example service methods.

*  ~~Is security a cross cutting concern? How is it implemented internally?~~
  Yes, AOP.

*  ~~What do @Secured and @RolesAllowed do? What is the difference between them?~~
  RolesAllowed is for role based security. Supply the annotation with an array of roles which are permitted to call the method.
  Secured is broader. Supports roles, but also concepts like FULLY_AUTHENTICATED.

*  ~~What is a security context?~~
  Object which holds information about security of app.

*  ~~In which order do you have to write multiple intercept-url's?~~
  * Most specific first.

*  ~~How is a Principal defined?~~
  * it is a user or a group or a system that accesses a resource.

*  ~~What is authentication and authorization? Which must come first?~~
  * Authentication is checking that someone can access a system.
  * authorization is checking that they can access a specific resource.
  * Authentication comes first.

*  ~~In which security annotation are you allowed to use SpEL?~~
  @PreAuthorize and @PostAuthorize


*  ~~Does Spring Security support password hashing? What is salting?~~
  * Yes.
  * Salting is the inclusion of a known string of characters in every value to be hashed. This prevents brute force attempts to find a hash that matches the given hash.

# ~~REST~~
*  ~~What does REST stand for?~~
  * Representational State Transfer

*  ~~What is a resource?~~
  * Object that can be returned from a restful method

*  ~~What are safe REST operations?~~
  * GET, HEAD, PUT

*  ~~What are idempotent operations? Why is idempotency important?~~
  * They can be called multiple times to give the same result (assuming no underlying changes). i.e they don't change the model themselves.

*  ~~Is REST scalable and/or interoperable?~~
  * Yes

*  What are the advantages of the RestTemplate?
  http://www.baeldung.com/rest-template


*  ~~Which HTTP methods does REST use?~~
  * GET, POST, DELETE, PUT

*  ~~What is an HttpMessageConverter?~~
  * Java class which converts http request to object for handling by an @RequestMapping method. Also used to convert the outgoing object to a response string.

*  ~~Is REST normally stateless?~~
  * Yes

*  ~~What does @RequestMapping do?~~
  * Establishes a mapping between a url and a method. Used for rest and MVC requests.

*  Is @Controller a stereotype? Is @RestController a stereotype?
  * Yes

*  ~~What is the difference between @Controller and @RestController?~~
  * They are essentially the same, but RestController does not lookup a view based on the returned string by default. i.e. it adds ResponseBody to @Controller

*  ~~When do you need @ResponseBody?~~
  * When you just want the response returned rather than looking up a view. Note that the response can be of any type as long as you have a HttpMessageConverter that is appropriate.

*  ~~What does @PathVariable do?~~
  * Allows access to a variable from the mapping path. For example If the mapping is this /thing/{id}, then this expression will get you access to the value in {} @PathVariable("id") Long id.


*  ~~What is the HTTP status return code for a successful DELETE statement?~~
  * 204 - no content

*  ~~What does CRUD mean?~~
  * Create read update delete

*  ~~Is REST secure? What can you do to secure it?~~
  * No
  * Secure the end point(s) with Spring Security

*  Where do you need @EnableWebMVC?

*  ~~Name some common http response codes. When do you need @ResponseStatus?~~
  * 200 - ok
  * 404 - not found
  * 204 - no content
  * @ResponseStatus is used when you want to override the default response code

*  ~~Does REST work with transport layer security (TLS)?~~
  * yes

*  Do you need Spring MVC in your classpath?


# ~~SPRING BOOT~~
* ~~What is Spring Boot?~~
  * a Way to get started with a production grade spring project with minimal setup. Assumes defaults, but everything is configurable.

*  ~~What are the advantages of using Spring Boot?~~
  * Minimum setup. Works out of the box. Assumes what you are using based on classpath content.

*  ~~Why is it “opinionated”?~~
  * Takes an opinion on what you want a component to do.

*  ~~How does it work? How does it know what to configure?~~
  * Scans the classpath and configures components it finds. Works well with starter poms, but does not rely on them. If you configure somethign that spring boot was going to configure then sb backs away.

*  ~~What things affect what Spring Boot sets up?~~
  * Contents of the classpath

*  ~~How are properties defined? Where?~~
  * Application.properties or application.yaml

*  ~~Would you recognize common Spring Boot annotations and configuration
properties if you saw them in the exam?~~
  * Yes


*  ~~What is the difference between an embedded container and a WAR?~~
  * embedded container includes an instance of tomcat (by default). It can be executed from the command line with java -jar (or java -cp something.jar??)
  * War file is traditional web archive. Can be deployed to any compliant container.

*  ~~What embedded containers does Spring Boot support?~~
  * Tomcat
  * Jetty


*  ~~What does @EnableAutoConfiguration do? What about @SpringBootApplication?~~
  * EnableAutoConfiguration tells spring boot to configure the application based on the jars found on the classpath. Works well with starter poms, but not directly related.
  This annotation is contained within @SpringBootApplication. Just include one EnableAutoConfiguration annotation in your app.
  * Example usage: if you have Hsqldb in your pom therefore in your classpath and don't have another db configured then EAC will set up an in memory db from this cp entry.
  * @SpringBootApplication is an amalgam of @EnableAutoConfiguration, @Configuration and @ComponentScan.


*  ~~What is a Spring Boot starter POM? Why is it useful?~~
  * Set of dependencies that you are likely to want when developing a particular type of app.
  * Spring boot starter parent is a set of maven configs including dependency management section. This section makes it possible to leave out the version number from related dependencies which have been blessed by spring.

*  ~~Spring Boot supports both Java properties and YML files. Would you recognize
and understand them if you saw them?~~
  * Yes

*  ~~Can you control logging with Spring Boot? How?~~
  * Yes
  * Add logging config details to the application.properties file or the yaml one.

*  ~~Note that the second Spring Boot section (Going Further) is not required for this
exam.~~

# MICROSERVICES
*  ~~What is a microservices architecture?~~
  * Build and deploy services independently. Use service registration and discovery to link the services together.


*  ~~What are the advantages and disadvantages of microservices?~~
  * Difficult to start with. Lots of moving parts. All need to work together.
  * Easier to move to. Carve off a piece and make it a m/s.
  * Simpler smaller deploys, each m/s takes care of itself including things like persistence.
  * Existing apps are monolithic and difficult to change.
  * Easier to scale with cheap hardware (mono app requires better hardware).
  * M/s can be more difficult to manage.
  * Can be multi language (both pro and con!)


*  What sub-projects of Spring Cloud did we cover in the course? Spring Cloud is a
large umbrella project – only what we covered in the course will be tested.

*  Would you recognize the Spring Cloud annotations and configuration we used in
the course if you saw it in the exam?

*  ~~What Netflx projects did we use?~~
  * Hystrix (circuit breaker)
  * Eureka (service discovery)
  * Ribbon (client side lb)
  * Zuul (intelligent routing)


*  How do you setup Service Discovery?
  * Add @EnableEurekaServer to the server application which will act at the service registry, configure this with application.yml or application.properties.
  * Add EnableDiscoveryClient to every microservice application (for registration). Configure the client with its own application.yml or application.properties.
  * ???


*  How do you access a RESTful microservice?

*  ~~What is Eureka?~~
  * It is a netflix service for looking up services (typically in AWS)
